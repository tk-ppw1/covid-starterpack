from django.shortcuts import render, redirect
from .models import ListKasus,KasusIndo
from . import models
from .forms import formKasus
from operator import itemgetter

# Create your views here.

def list_kasus(request):
    list_kasus = ListKasus.objects.all()
    
    kasus = KasusIndo.objects.all()

    a = []
    a.append(kasus)
    if all(x for x in a) == False:
        models.KasusIndo.objects.create(nama='Indonesia',jumlah_total='463007',kasus_sembuh='388094',kasus_meninggal='15148')
    else : 
        pass
    kasus = KasusIndo.objects.all()
    context = {'list_kasus':list_kasus,'kasus':kasus}
    return render(request,'list_kasus/listkasus.html',context)


def kasus_detail(request, pk):
    kasus = ListKasus.objects.all().filter(id=pk)
    list_kasus = ListKasus.objects.all()
    context = {'kasus': kasus,'list_kasus':list_kasus}
    return render(request,'list_kasus/kasusdetail.html',context)

def tambah_kasus(request):
    form = formKasus()
    if(form.is_valid and request.method == 'POST'):
        form = formKasus(request.POST)
        form.save()
        return redirect('/daftarkasus/')
    
    context = {'form':form}

    return render(request,'list_kasus/tambahkasus.html',context)
