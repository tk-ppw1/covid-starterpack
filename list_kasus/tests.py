from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from . import models
from . import views
from . import apps
# Create your tests here.

class Tests(TestCase): 
    def test_kasus_url_i(self):
        response = Client().get('/daftarkasus/')
        self.model_pk = models.KasusIndo.objects.get(pk=1)
        aww = models.KasusIndo.objects.filter(nama="Indonesia").count()
        self.assertEqual(aww,1)
        self.assertEqual(str(self.model_pk), "Indonesia")
        
    def setUp(self):
        self.kasus = models.ListKasus.objects.create(nama_daerah='a',jumlah_total='5',kasus_sembuh='5',kasus_meninggal='0')
       

    def test_str(self):
        self.assertEqual(str(self.kasus), "a")
        

    def test_kasus_url_is_resolved(self):
        response = Client().get('/daftarkasus/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_is_resolved2(self):
        response = Client().get('/daftarkasus/tambahkasus/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_is_resolved3(self):
        response = Client().get('/daftarkasus/kasus_detail/1/')
        self.assertEquals(response.status_code, 200)

    def test_form_save_a_POST_request(self):
        response = Client().post('/daftarkasus/tambahkasus/', data={'nama_daerah':'z','jumlah_total':'5','kasus_sembuh':'5','kasus_meninggal':'0'})
        jumlah = models.ListKasus.objects.filter(nama_daerah='z').count()
        self.assertEqual(jumlah,1)


    def testConfig(self):
        self.assertEqual(apps.ListKasusConfig.name, 'list_kasus')
