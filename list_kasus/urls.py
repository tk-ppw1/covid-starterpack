from django.urls import path
from . import views
app_name = 'list_kasus'

urlpatterns = [

    path('',views.list_kasus,name = 'list_kasus'),
    path('kasus_detail/<int:pk>/',views.kasus_detail, name = 'kasusdetail'),
    path('tambahkasus/', views.tambah_kasus, name='tambahkasus')
]