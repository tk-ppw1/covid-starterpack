from django.test import TestCase, Client
from django.urls import resolve
from .views import barang, daftar_barang, daftar_pesanan, berhasil
from .models import Barang, Pesanan
from .apps import CovidKitConfig

# Create your tests here.
class TestNamaApp(TestCase):
	def test_nama_app(self):
		self.assertEqual(CovidKitConfig.name, 'covid_kit')

class TestModel(TestCase):
	def test_event_model_create_barang_object(self):
		requestorang = Barang(nama="light stick", deskripsi="bagus", foto="asdfghjkl")
		requestorang.save()
		self.assertEqual(Barang.objects.all().count(), 1)
		
	def test_event_model_create_pesanan_object(self):
		pesananorang = Pesanan(nama_pemesan="evita", alamat="jalan yang benar", no_hp="081234567890", pesanan="hand sanitizer")
		pesananorang.save()
		self.assertEqual(Pesanan.objects.all().count(), 1)

class TestViews(TestCase):
	def test_event_using_template(self):
		response = Client().get('/covid_kit/')
		self.assertTemplateUsed(response, 'kit.html')
	
	def test_menggunakan_template_berhasil(self):
		response = Client().get('/covid_kit/berhasil/')
		self.assertEqual(response.status_code, 200)
	
	def test_bisa_request_barang(self):
		data = {
			"nama":"light stick",
			"deskripsi":"bagus",
			"foto":"asdfghjkl"
		}
		response = Client().post('/covid_kit/daftar/', data=data)

		count = Barang.objects.count()

		self.assertEqual(response.status_code, 302)
	
	def test_bisa_pesan_barang(self):
		data = {
			"nama_pemesan":"haje",
			"alamat":"jalan yang benar",
			"no_hp":"081234567890",
			"pesanan":"hand sanitizer 100 pcs"
		}
		response = Client().post('/covid_kit/pesan/', data=data)

		count = Pesanan.objects.count()

		self.assertEqual(response.status_code, 302)

class TestUrls(TestCase):
	def test_apakah_url_covid_kit_ada(self):
		response = Client().get('/covid_kit/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_url_daftar_ada(self):
		response = Client().get('/covid_kit/daftar/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_url_pesan_ada(self):
		response = Client().get('/covid_kit/pesan/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_berhasil_ada(self):
		found = resolve('/covid_kit/berhasil/')
		self.assertEqual(found.func, berhasil)
    


