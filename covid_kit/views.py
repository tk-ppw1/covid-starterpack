from django.shortcuts import render, HttpResponseRedirect
from .models import Barang, Pesanan
from .forms import BarangForm, PesanForm

# Create your views here.
def barang(request):
    return render(request, "kit.html")

def berhasil(request):
    return render(request, "berhasil.html")

def daftar_barang(request):
    form = BarangForm()
    if request.method == 'POST':
        form = BarangForm(request.POST)

        Barang.objects.create(
            nama = request.POST['nama'],
            deskripsi = request.POST['deskripsi'],
            foto = request.POST['foto']
        )

        return HttpResponseRedirect('/covid_kit/daftar')
    
    barangList = Barang.objects.all()
    response = {
        'form':form,
        'barangs':barangList
    }
    return render(request, "form.html", response)

def daftar_pesanan(request):
    form = PesanForm()
    if request.method == 'POST':
        form = PesanForm(request.POST)

        Pesanan.objects.create(
            nama_pemesan = request.POST['nama_pemesan'],
            alamat = request.POST['alamat'],
            no_hp = request.POST['no_hp'],
            pesanan = request.POST['pesanan']
        )

        return HttpResponseRedirect('/covid_kit/berhasil')
    
    response = {
        'form':form
    }
    return render(request, "formpesan.html", response)