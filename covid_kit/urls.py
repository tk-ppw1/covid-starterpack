from django.urls import path, include
from .views import barang, daftar_barang, daftar_pesanan, berhasil
app_name = 'covid_kit'
urlpatterns = [
    path('',barang, name="covid_kit"),
    path('daftar/',daftar_barang, name='daftar'),
    path('pesan/',daftar_pesanan, name='pesan'),
    path('berhasil/',berhasil, name='berhasil')
]