from django.test import TestCase, Client
from django.urls import reverse
# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url_test = reverse("contact:contact")

    def test_homepage_url(self):
        response = self.client.get(self.homepage_url_test)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "Contact_view.html")