from django import forms

class FormPertanyaan(forms.Form):
    pertanyaan = forms.CharField(
        widget = forms.TextInput(attrs={'placeholder':'Ask your question here...', 'style':'width:30%; border-radius:5px'})
    )

class FormJawaban(forms.Form):
    pengisi = forms.CharField(
        label = 'Dijawab oleh:',
        widget = forms.TextInput(attrs={'style':'width:100%'})
    )
    jawaban = forms.CharField(
        widget = forms.Textarea(attrs={'style':'width:100%'})
    )