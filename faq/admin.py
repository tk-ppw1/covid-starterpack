from django.contrib import admin

# Register your models here.
from .models import ModelJawaban, ModelPertanyaan

admin.site.register(ModelJawaban)
admin.site.register(ModelPertanyaan)