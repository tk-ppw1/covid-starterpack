from django.test import TestCase, Client
from .models import ModelJawaban, ModelPertanyaan
from .forms import FormJawaban, FormPertanyaan
from .views import faq, list_pertanyaan, list_faq, jawab, detail
from django.urls import resolve, reverse
from .apps import FaqConfig
from django.apps import apps

# Create your tests here.
class TestUrls(TestCase):
    def test_apakah_url_faq_ada(self):
        response = Client().get('/faq/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_list_pertanyaan_ada(self):
        response = Client().get('/faq/list-pertanyaan/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_list_faq_ada(self):
        response = Client().get('/faq/list-FAQ/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_jawab_ada(self):
        response = Client().get('/faq/jawab/P8/')
        self.assertEqual(response.status_code, 200)

class TestViews(TestCase):
    def test_views_faq(self):
        found = resolve('/faq/')
        self.assertEqual(found.func, faq)

    def test_views_list_pertanyaan(self):
        found = resolve('/faq/list-pertanyaan/')
        self.assertEqual(found.func, list_pertanyaan)

    def test_views_jawab(self):
        found = resolve('/faq/jawab/P8/')
        self.assertEqual(found.func, jawab)

    def test_views_list_faq(self):
        found = resolve('/faq/list-FAQ/')
        self.assertEqual(found.func, list_faq)

    def test_views_detail(self):
        found = resolve('/faq/detail/P8/')
        self.assertEqual(found.func, detail)
    
    def test_redirect_url_setelah_isi_pertanyaan(self):
        response = Client().get('/faq/list-pertanyaan')
        self.assertEqual(response.status_code, 301)
    
    def test_redirect_url_setelah_isi_jawaban(self):
        response = Client().get('/faq/list-FAQ')
        self.assertEqual(response.status_code, 301)

class TestHTML(TestCase):
    def test_faq_templates_is_used(self):
        response = Client().get('/faq/')
        self.assertTemplateUsed(response, 'faq/faq.html')

    def test_list_pertanyaan_templates_is_used(self):
        response = Client().get('/faq/list-pertanyaan/')
        self.assertTemplateUsed(response, 'faq/list-pertanyaan.html')
    
    def test_list_faq_templates_is_used(self):
        response = Client().get('/faq/list-FAQ/')
        self.assertTemplateUsed(response, 'faq/list-faq.html')
    
    def test_jawab_templates_is_used(self):
        response = Client().get('/faq/jawab/P8/')
        self.assertTemplateUsed(response, 'faq/jawab.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(FaqConfig.name, 'faq')
        self.assertEqual(apps.get_app_config('faq').name, 'faq')

class TestModel(TestCase):
    def setUp(self):
        objek_pertanyaan = ModelPertanyaan(pertanyaan="Apakah menggunakan masker wajib?")
        objek_pertanyaan.save()

        objek_jawaban = ModelJawaban(jawaban="Wajib")
        objek_jawaban.save()

    def test_instance_created(self):
        self.assertEqual(ModelJawaban.objects.all().count(), 1)
        self.assertEqual(ModelPertanyaan.objects.all().count(), 1)