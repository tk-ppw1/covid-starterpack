from django import forms
from .models import RumahSakit, PendaftarRapid, PendaftarSwab

class Input_RS(forms.ModelForm):
    class Meta:
        model = RumahSakit
        fields = ['nama', 'provinsi', 'kabkota', 'alamat', 'telepon']
    
        labels = {
            'nama' : 'Nama Rumah Sakit:',
            'provinsi' : 'Provinsi:',
            'kabkota' : 'Kabupaten / Kota:',
            'alamat' : 'Alamat:',
            'telepon' : 'No. Telepon:',
        }
        input_attrs = {
            'type' : 'text',
            'class' : 'form-control'
        }
        widgets = {
            'nama' : forms.TextInput(attrs=input_attrs),
            'provinsi' : forms.TextInput(attrs=input_attrs),
            'kabkota' : forms.TextInput(attrs=input_attrs),
            'alamat' : forms.Textarea(attrs=input_attrs),
            'telepon' : forms.TextInput(attrs=input_attrs),
        }

class Input_Tes(forms.Form):
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'class' : 'form-control'
    }

    nama = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    no_id = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    telepon = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    alamat = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs))
    tempat = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    
    TESCOVID = (
        ('1', 'Rapid Test'),
        ('2', 'Swab Test'),
    )
    tes = forms.ChoiceField(choices=TESCOVID, label='', required=True, widget=forms.Select(attrs=input_attrs))
