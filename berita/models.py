from django.db import models
from django.shortcuts import reverse


class Berita(models.Model):
    title = models.TextField(unique=True)
    image_url = models.TextField()
    content = models.TextField()


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        link = self.title.replace(" ","%20")
        return reverse("berita:detailPost",args=[link])


