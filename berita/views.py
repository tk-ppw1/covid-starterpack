from django.shortcuts import get_object_or_404, redirect, render
from .forms import BeritaForm
from .models import Berita

# Create your views here.
def pageBerita(request):
    context = {
        "listBerita" : Berita.objects.all().order_by("title"),
    }
    return render(request, 'PageBerita_view.html', context)


def makePost(request):
    if request.method == 'POST':
        beritaForm = BeritaForm(request.POST)
        if beritaForm.is_valid():
            beritaForm.save()
        return redirect("berita:pageBerita")
    else:
        return render(request,"MakePost_view.html")

def detailPost(request, title):
    title = title.replace("%20"," ")
    berita = get_object_or_404(Berita,title=title)
    context = {
        "berita" : berita,
    }
    return render(request,"DetailPost_view.html",context)

def deletePost(request, title):
    if request.method == "POST":
        title = title.replace("%20"," ")
        berita = Berita.objects.get(title=title)
        berita.delete()
    return redirect("berita:pageBerita")

